package com.mocha.T543二叉树的直径;

public class Solution {
    int ans;
    public int diameterOfBinaryTree(TreeNode root) {
        ans=1;
        dfomial(root);
        return ans-1;

    }
    public int dfomial(TreeNode root) {
        if (root==null){
            return 0;
        }else {
            int L = dfomial(root.left);
            int R = dfomial(root.right);
            ans=Math.max(ans,L+R+1);
            return  Math.max(L, R) + 1;
        }
    }

}